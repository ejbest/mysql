#!/bin/bash
set -x

cd /vol1/database_details


for i  in 1 2 3 4 5
do

mysqldump –h 127.0.0.1 –u sa –pinitial123 ejs –tables deploys > deploys.sql
mysqldump –h 127.0.0.1 –u sa -pinitial123 ejs –tables tickets > tickets.sql
if [ "$?" -eq 0 ]
then
    echo "Success"
    break
else
    echo "Mysqldump encountered a problem"
fi

done

#push data to git

git add -A

git commit -m "$(date +"%Y_%m_%d_%I_%M_%p")"

git push origin database_details


exit 0

