set -x 

#!/bin/bash
# Shell script to install mysql-client and create cron job


cd /vol1/mysql
#git clone git@bitbucket.org:ejbest/mysql.git

#change this command if you need to install it in different way
sudo yum install mysql -y
yum install cronie -y
cd /vol1
rm -Rf database_details
git clone git@bitbucket.org:ejbest/database_details.git
cd /vol1/database_details

#loadmysql data - try 5 times until fails

for i  in 1 2 3 4 5
do
mysql  -h 127.0.0.1 -u sa -pinitial123 ejs < deploys.sql
mysql  -h 127.0.0.1 -u sa -pinitial123 ejs < tickets.sql

if [ "$?" -eq 0 ]
then
    echo "Success"
    break
else
    echo "Mysqldump encountered a problem"
fi

done

#setcron hourly

sudo cp /vol1/mysql/secondscript.sh /etc/cron.hourly

sudo chmod -x  /etc/cron.hourly/secondscript.sh
/etc/init.d/crond restart

exit 0
